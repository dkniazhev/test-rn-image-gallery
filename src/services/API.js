// @flow
const API_KEY = '0e2a751704a65685eefc' // other valid API keys: '760b5fb497225856222a', '0e2a751704a65685eefc'
const API_ENDPOINT = 'http://195.39.233.28:8035'
const AUTH = '/auth'
const IMAGES = '/images'

export type AuthToken = string

const contentType = {
  'Content-Type': 'application/json',
}

export async function getToken (): AuthToken {
  try {
    const response = await fetch(API_ENDPOINT + AUTH, {
      method: 'POST',
      headers: { ...contentType },
      body: JSON.stringify({
        'apiKey': API_KEY,
      }),
    })
    checkResponse(response)
    const json = await response.json()
    return json.token
  } catch (e) {
    console.log(e)
  }
}

function getHeaders (token: AuthToken) {
  return {
    'Authorization': `Bearer ${token}`,
    ...contentType,
  }
}

function checkResponse (response) {
  if (!response.ok) {
    const error = new Error(response)
    error.status = response.status
    throw error
  }
}

async function awaitAndCheck (response) {
  checkResponse(response)
  return response.json()
}

export async function getPictures (token: AuthToken, page: number = 1): Array<Object> {
  // http://195.39.233.28:8035/images?page=xxx
  const response = await fetch(`${API_ENDPOINT + IMAGES}?page=${page}`, {
    headers: getHeaders(token),
  })
  return awaitAndCheck(response)
}

export async function getPictureDetails (token: AuthToken, id: number): Object {
  // http://195.39.233.28:8035/images/id
  const response = await fetch(`${API_ENDPOINT + IMAGES}/${id}`, {
    headers: getHeaders(token),
  })
  return awaitAndCheck(response)
}
