// @flow

export function isUnauthorized (error) {
  return error.status === 401
}
