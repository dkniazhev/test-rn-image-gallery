// @flow
import * as React from 'react'
import {
  View,
  Image,
  Dimensions,
  Text,
  ActivityIndicator,
} from 'react-native'

import styles from './styles'
import commonStyles from '../../styles/commonStyles'
import DetailsFooter from './components/DetailsFooter'
import ImageZoom from 'react-native-image-pan-zoom'

type Props = {
  imageUrl: string,
  isLoading: boolean,
  shareCallback: Function,
  applyFilterCallback: Function,
  pictureDetails: Object,
  errorMessage: string
}

class DetailView extends React.PureComponent<Props> {
  render () {
    const { imageUrl, isLoading, errorMessage, shareCallback, applyFilterCallback, pictureDetails } = this.props
    return (
      <View style={styles.container}>
        {isLoading && <ActivityIndicator size={'large'} />}
        {!!errorMessage && <Text style={commonStyles.errorMessage}>{errorMessage}</Text>}
        <View style={styles.imageContainer}>
          <ImageZoom cropWidth={Dimensions.get('window').width}
            cropHeight={Dimensions.get('window').height}
            imageWidth={styles.imageStyle.width}
            imageHeight={styles.imageStyle.height}>
            <Image
              source={{ uri: imageUrl }}
              style={styles.imageStyle} />
          </ImageZoom>
        </View>
        <Text style={styles.authorName}>{pictureDetails.author}</Text>
        <Text style={styles.cameraModel}>{pictureDetails.camera}</Text>
        <DetailsFooter
          pictureDetails={pictureDetails}
          shareCallback={shareCallback}
          applyFilterCallback={applyFilterCallback}
        />
      </View>
    )
  }
}

export default DetailView
