import { Dimensions, StyleSheet } from 'react-native'
const { width } = Dimensions.get('window')

const styles: any = StyleSheet.create({
  container: {
    backgroundColor: '#000',
    flex: 1,
  },
  imageContainer: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  imageStyle: {
    width: width * 0.9,
    height: width * 0.9,
  },
  backButton: {
    position: 'absolute',
    left: 5,
    top: 5,
  },
  spinner: {
    position: 'absolute',
  },
  detailView: {
    position: 'absolute',
    bottom: 10,
    width: 50,
    right: 20,
    flexDirection: 'row',
  },
  detailViewImage: {
    width: 50,
    height: 50,
  },
  authorName: {
    position: 'absolute',
    bottom: 40,
    left: 20,
    fontSize: 20,
    fontWeight: '900',
    color: '#FFF',
  },
  cameraModel: {
    position: 'absolute',
    bottom: 10,
    left: 20,
    fontSize: 20,
    fontWeight: '600',
    color: '#FFF',
  },
})
export default styles
