import * as React from 'react'
import {
  TouchableOpacity,
  Image,
  View,
} from 'react-native'
import styles from '../styles'
import shareImage from './images/ShareThis.png'

type Props = {
  shareCallback: Function,
  pictureDetails: Object,
}

class DetailsFooter extends React.PureComponent<Props> {
  render () {
    const { shareCallback, pictureDetails } = this.props
    if (!pictureDetails) return null
    const imageId = pictureDetails.id
    return (
      <View style={styles.detailView}>
        <TouchableOpacity
          style={{ alignSelf: 'flex-start' }}
          onPress={() => shareCallback(imageId)}
        >
          <Image style={styles.detailViewImage}
            resizeMode='cover'
            source={shareImage} />
        </TouchableOpacity>
      </View>
    )
  }
}

export default DetailsFooter
