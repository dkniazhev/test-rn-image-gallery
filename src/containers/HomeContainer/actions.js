import { getPictures, getToken } from '../../services/API'
import type { ActionWithPayload, ActionWithoutPayload, PicturesList } from '../../types/actions'
import type { AuthToken } from '../../services/API'
import { isUnauthorized } from '../../util'

export const FETCH_REQUESTED = 'FETCH_REQUESTED'
export const FETCH_FAILED = 'FETCH_FAILED'
export const FETCH_TOKEN_FAILED = 'FETCH_TOKEN_FAILED'
export const PICTURES_FETCH_SUCCESS = 'PICTURES_FETCH_SUCCESS'
export const FETCH_TOKEN_SUCCESS = 'FETCH_TOKEN_SUCCESS'

export function isLoading (): ActionWithoutPayload {
  return {
    type: FETCH_REQUESTED,
  }
}

export function fetchListSuccess (pictures: PicturesList): ActionWithPayload {
  return {
    type: PICTURES_FETCH_SUCCESS,
    payload: pictures,
  }
}

export function fetchFailed (errorMessage: string): ActionWithPayload {
  return {
    type: FETCH_FAILED,
    errorMessage: errorMessage,
  }
}

export function fetchTokenFailed (errorMessage: string): ActionWithPayload {
  return {
    type: FETCH_TOKEN_FAILED,
    errorMessage: errorMessage,
  }
}

export function fetchTokenSuccess (token: AuthToken): ActionWithPayload {
  return {
    type: FETCH_TOKEN_SUCCESS,
    payload: token,
  }
}

export function initialFetchPictures () {
  return async (dispatch, getState) => {
    await fetchToken()(dispatch, getState)
    fetchPictures()(dispatch, getState)
  }
}

export function fetchPictures (page: number = 1) {
  return async (dispatch, getState) => {
    if (getState().homeReducer.isLoading) {
      return
    }
    dispatch(isLoading())
    try {
      const picturesPayload = await getPictures(getState().homeReducer.token, page)
      dispatch(fetchListSuccess(picturesPayload))
    } catch (e) {
      if (isUnauthorized(e)) {
        await fetchToken()(dispatch, getState)
        fetchPictures(page)(dispatch, getState)
        return
      }
      dispatch(fetchFailed(e))
    }
  }
}

export function fetchToken () {
  return async dispatch => {
    dispatch(isLoading())
    try {
      const token = await getToken()
      dispatch(fetchTokenSuccess(token))
    } catch (e) {
      dispatch(fetchTokenFailed(e))
    }
  }
}
