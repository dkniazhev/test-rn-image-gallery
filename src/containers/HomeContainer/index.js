// @flow
import * as React from 'react'
import { Platform, StatusBar } from 'react-native'
import { connect } from 'react-redux'

import HomeView from '../../screens/Home'
import { fetchPictures, initialFetchPictures } from './actions'

export interface Props {
  navigation: any,
  fetchPictures: Function,
  initialFetchPictures: Function,
  pictures: Array<Object>,
  isLoading: boolean,
  page: number,
  hasMore: boolean,
  errorMessage: string
}

export interface State {}

class HomeContainer extends React.Component<Props, State> {
  static navigationOptions = {
    header: null,
  }

  constructor (props) {
    super(props)
    StatusBar.setBarStyle('light-content')
    Platform.OS === 'android' && StatusBar.setBackgroundColor('#000')
    this.onRefresh = this.onRefresh.bind(this)
    this.onLoadNext = this.onLoadNext.bind(this)
  }

  componentDidMount () {
    this.props.initialFetchPictures()
  }

  onRefresh (): void {
    this.props.fetchPictures(1)
  }

  onLoadNext (): void {
    if (this.props.hasMore) {
      this.props.fetchPictures(this.props.page)
    }
  }

  render () {
    return <HomeView {...this.props}
      onRefresh={this.onRefresh}
      onLoadNext={this.onLoadNext} />
  }
}

function bindAction (dispatch) {
  return {
    fetchPictures: page => {
      return dispatch(fetchPictures(page))
    },
    initialFetchPictures: () => {
      return dispatch(initialFetchPictures())
    },
  }
}

const mapStateToProps = state => ({
  pictures: state.homeReducer.pictures,
  page: state.homeReducer.page,
  hasMore: state.homeReducer.hasMore,
  isLoading: state.homeReducer.isLoading,
  errorMessage: state.homeReducer.errorMessage,
})

export default connect(mapStateToProps, bindAction)(HomeContainer)
