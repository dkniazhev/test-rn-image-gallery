// @flow
import type { ActionWithPayload } from '../../types/actions'
import {
  FETCH_REQUESTED,
  PICTURES_FETCH_SUCCESS,
  FETCH_FAILED,
  FETCH_TOKEN_SUCCESS, FETCH_TOKEN_FAILED,
} from './actions'

const initialState = {
  token: '',
  pictures: [],
  isLoading: true,
  page: 1,
  hasMore: false,
  errorMessage: '',
}

export default function (state: any = initialState, action: ActionWithPayload) {
  const payload = action.payload

  let propertiesToChange = {}

  switch (action.type) {
    case FETCH_REQUESTED: {
      propertiesToChange = {
        isLoading: true,
      }
      break
    }
    case FETCH_TOKEN_SUCCESS: {
      propertiesToChange = {
        token: payload,
        isLoading: false,
      }
      break
    }
    case PICTURES_FETCH_SUCCESS: {
      propertiesToChange = {
        pictures: state.pictures.concat(payload.pictures),
        page: payload.pictures.length > 0 ? state.page + 1 : state.page,
        isLoading: false,
        hasMore: payload.hasMore,
      }
      break
    }
    case FETCH_FAILED:
    case FETCH_TOKEN_FAILED:
    {
      propertiesToChange = {
        errorMessage: payload,
        isLoading: false,
      }
      break
    }
  }
  return Object.assign({}, state, propertiesToChange)
}
