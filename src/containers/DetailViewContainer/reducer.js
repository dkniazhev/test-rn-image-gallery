import {
  PICTURE_DETAILS_FETCH_REQUESTED,
  PICTURE_DETAILS_FETCH_SUCCESS,
} from './actions'

import {
  FETCH_FAILED,
} from '../HomeContainer/actions'

const initialState = {
  hiResPictures: [],
  isLoading: false,
  errorMessage: '',
}

export default function (state: any = initialState, action: Object) {
  const payload = action.payload
  let propertiesToChange = {}

  switch (action.type) {
    case PICTURE_DETAILS_FETCH_REQUESTED: {
      propertiesToChange = {
        isLoading: true,
      }
      break
    }
    case PICTURE_DETAILS_FETCH_SUCCESS: {
      propertiesToChange = {
        hiResPictures: !state.hiResPictures.find(hiResPic => hiResPic.id === payload.id)
          ? state.hiResPictures.concat(payload)
          : state.hiResPictures,
        isLoading: false,
        errorMessage: '',
      }
      break
    }
    case FETCH_FAILED: {
      propertiesToChange = {
        errorMessage: payload,
        isLoading: false,
      }
      break
    }
  }
  return Object.assign({}, state, propertiesToChange)
}
