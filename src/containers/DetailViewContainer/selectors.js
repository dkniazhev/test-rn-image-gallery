// @flow

export const selectHiResImage = (state: Object, imageId: number) => {
  return state.detailViewReducer.hiResPictures
    .find(hiResPic => hiResPic.id === imageId)
}
