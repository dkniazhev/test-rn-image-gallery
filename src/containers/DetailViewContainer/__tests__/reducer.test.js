import reducer from '../reducer'

describe('list reducer', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual({
      hiResPictures: [],
      isLoading: false,
      errorMessage: '',
    })
  })
})
