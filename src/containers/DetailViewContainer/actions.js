// @flow

import { getPictureDetails } from '../../services/API'
import { FETCH_FAILED, fetchToken } from '../HomeContainer/actions'
import type { ActionWithPayload, ActionWithoutPayload, FullPicture } from '../../types/actions'
import { isUnauthorized } from '../../util'

export const PICTURE_DETAILS_FETCH_REQUESTED = 'PICTURE_DETAILS_FETCH_REQUESTED'
export const PICTURE_DETAILS_FETCH_SUCCESS = 'PICTURE_DETAILS_FETCH_SUCCESS'

export function pictureIsLoading (): ActionWithoutPayload {
  return {
    type: PICTURE_DETAILS_FETCH_REQUESTED,
  }
}

export function fetchPictureSuccess (imageId: number, hiResImage: FullPicture): ActionWithPayload {
  return {
    type: PICTURE_DETAILS_FETCH_SUCCESS,
    payload: hiResImage,
  }
}

export function fetchPictureFailed (errorMessage: string): ActionWithPayload {
  return {
    type: FETCH_FAILED,
    payload: errorMessage,
  }
}

export function fetchPictureDetails (id: number) {
  return async (dispatch, getState) => {
    dispatch(pictureIsLoading())
    try {
      const picturePayload = await getPictureDetails(getState().homeReducer.token, id)
      dispatch(fetchPictureSuccess(id, picturePayload))
    } catch (e) {
      if (isUnauthorized(e)) {
        await fetchToken()(dispatch, getState)
        fetchPictureDetails(id)(dispatch, getState)
        return
      }
      dispatch(fetchPictureFailed(e))
    }
  }
}
