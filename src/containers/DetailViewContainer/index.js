// @flow
import * as React from 'react'
import DetailView from '../../screens/DetailView'
import { connect } from 'react-redux'
import { fetchPictureDetails } from './actions'
import { selectHiResImage } from './selectors'
import { Share } from 'react-native'

export interface Props {
  navigation: any,
  fetchPictureDetails: Function,
  isLoading: boolean,
  hiResImage: Function,
  errorMessage: string
}
export interface State {
  imageUrl: string,
}

class DetailViewContainer extends React.Component<Props, State> {
  static navigationOptions = {
    headerTransparent: {
      backgroundColor: 'transparent',
      position: 'absolute',
      height: 50,
      top: 0,
      left: 0,
      right: 0,
      borderBottomWidth: 0,
    },
    headerTintColor: '#FFF',
  }

  componentDidMount () {
    const { navigation, fetchPictureDetails } = this.props
    const { pictureDetails } = navigation.state.params
    if (!this.props.hiResImage(pictureDetails.id)) {
      fetchPictureDetails(pictureDetails.id)
    }
  }

  share = async (imageId: number): void => {
    const image = this.props.hiResImage(imageId)

    if (!image) {
      return
    }
    try {
      await Share.share({
        message: 'Share image',
        content: {
          url: image.full_picture,
        },
      })
    } catch (e) {
      console.log(e)
    }
  }

  applyFilter = (type): void => {
    // TODO: implement apply image filter function
  }

  render () {
    const { pictureDetails } = this.props.navigation.state.params
    const { isLoading, errorMessage } = this.props
    const image = this.props.hiResImage(pictureDetails.id)
    return (!!image && !this.props.isLoading && <DetailView
      imageUrl={image.full_picture}
      pictureDetails={image}
      shareCallback={this.share}
      isLoading={isLoading}
      errorMessage={errorMessage}
      applyFilterCallback={this.applyFilter}
    />)
  }
}

function bindAction (dispatch) {
  return {
    fetchPictureDetails: imageId => dispatch(fetchPictureDetails(imageId)),
  }
}

const mapStateToProps = state => ({
  hiResImage: imageId => selectHiResImage(state, imageId),
  isLoading: state.detailViewReducer.isLoading,
  errorMessage: state.detailViewReducer.errorMessage,
})

export default connect(mapStateToProps, bindAction)(DetailViewContainer)
