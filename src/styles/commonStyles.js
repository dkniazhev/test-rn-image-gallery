import { StyleSheet } from 'react-native'

const commonStyles: any = StyleSheet.create({
  errorMessage: {
    fontSize: 20,
    fontWeight: '900',
    color: '#FF0000',
    textAlign: 'center',
  },
})
export default commonStyles
