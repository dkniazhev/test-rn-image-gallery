// @flow
export type ActionWithPayload = {
  type: string,
  payload: Object,
}

export type ActionWithoutPayload = {
  type: string,
}

export type SmallPicture = {
  id: string,
  cropped_picture: string
}

export type FullPicture = {
  id: string,
  cropped_picture: string,
  full_picture: string,
  camera: string,
  author: string
}

export type PicturesList = {
  pictures: Array<SmallPicture>,
  hasMore: boolean,
  page: number
}
