##Test image gallery
###Missing features/known issues:
* Filters are missing
* Swiping is not implemented
* The list flickers
* Some tests have been removed
###Missing nice-to-haves
* Reselect for selectors
* Flow
* Underscore for maps
* Offline mode
###How to build and test
`yarn install`

`yarn test`

`yarn lint`

###How to run
####iOS

`react-native run-ios`

####Android

`react-native run-android`